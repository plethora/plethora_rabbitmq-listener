<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | RabbitMQ Listener

This is a simple rabbitmq message service that push every received message to a mongo collection.


# | Tests

To run tests, please run `test/push_messages` in a first terminal, and `dist/index.js` in a second.
`push_messages` sends messages to the reader that should display them in the terminal.
