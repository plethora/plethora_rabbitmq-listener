import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();

if (error) {
    throw error;
}

import { connect as connectAmqp, Message, Channel } from 'amqplib';
import { MongoClient, Db } from 'mongodb';
import { IMessage } from '@plethora/types';
import { AmqpConnection } from '@plethora/utils';

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const amqpConnection = new AmqpConnection(amqpConnectionConf);
const q = process.env.AMQP_QUEUE || 'plethora';

const mongoUrl = process.env.MONGO_URL || 'mongodb://localhost:27017/PLETHORA';
const MESSAGES_COLLECTION = 'messages';

const PLETHORA_TOPIC_EXCHANGE = 'plethora_topic';
const PLETHORA_TOPIC_PREFIX = 'plethora';

MongoClient.connect(mongoUrl)
    .then((db: Db) => Promise.all([
        db,
        amqpConnection.retrieveQueueMessage(q),
    ]))
    .then(([db, msgObservable]) => {
        console.log('listen...');
        msgObservable.subscribe((msg: Message) => {
            if (!msg) return;
            const parsedMsg: IMessage = JSON.parse(msg.content.toString());
            console.log('[RABBITMQ_LISTENER]#plethora received message', parsedMsg);
            const PLETHORA_TOPIC_NAME = `${PLETHORA_TOPIC_PREFIX}.${parsedMsg.data.key}`;
            db.collection(MESSAGES_COLLECTION)
                .insert(parsedMsg)
                .then(() => amqpConnection.createChannel())
                .then((channel: Channel) => channel.ack(msg))
                .then(() =>
                    amqpConnection.publishOnTopic(
                        PLETHORA_TOPIC_EXCHANGE,
                        PLETHORA_TOPIC_NAME,
                        (msg as Message).content,
                    ),
                )
                .then(() => {
                    console.log(
                        '[RABBITMQ_LISTENER]#plethora_topic Sent message to %s',
                        PLETHORA_TOPIC_NAME,
                    );
                })
                .catch((err) => { console.error(err); });
        });
    })
    .catch(err => console.error(err));
