import { config as loadConfigAsEnvVar } from 'dotenv';
const { error } = loadConfigAsEnvVar();
if (error) {
    throw error;
}
import { connect as connectAmqp } from 'amqplib';
import { v4 } from 'uuid';

import { clearTimeout } from 'timers';

const amqpConnectionConf = {
    protocol: 'amqp',
    hostname: process.env.PLETHORA_IP,
    port: Number(process.env.RABBITMQ_PORT),
    username: process.env.RABBITMQ_USER,
    password: process.env.RABBITMQ_PASS,
    vhost: process.env.RABBITMQ_VHOST,
};
const q = process.env.AMQP_QUEUE || 'plethora';
const UUID = v4();

let timeout: NodeJS.Timer;

connectAmqp(amqpConnectionConf)
    .then(amqpConnection => amqpConnection.createChannel())
    .then(channel => Promise.all([channel, channel.assertQueue(q)]))
    .then(([channel]) => {
        const toDo = () => {
            const toSend = {
                device_id: UUID,
                module_id: 'DEFAULT',
                timestamp: new Date().getTime(),
                data: {
                    key: 'HEARTBEAT',
                    value: 'UP',
                },
            };
            console.log('send', toSend);
            channel.sendToQueue(q, new Buffer(JSON.stringify(toSend)));
            timeout = setTimeout(toDo, 500);
        };

        toDo();
    });

process.on('SIGINT', () => {
    clearTimeout(timeout);
    process.exit();
});
